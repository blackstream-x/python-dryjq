# -*- coding: utf-8 -*-

"""

tests.data

Unit test data

Copyright (C) 2022 Rainer Schwarzbach

This file is part of dryjq.

dryjq is free software: you can redistribute it and/or modify
it under the terms of the MIT License.

dryjq is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the LICENSE file for more details.

"""


RETURNCODE_OK = 0
RETURNCODE_ERROR = 1

INPUT_YAML_ANIMALS = """felines:
  cats:
    big:
    - lion
    - tiger
    - jaguar
    small:
    - ocelot
    - domestic cat
canines:
  dogs:
  - wolf
  - dog
  - fox
  bears:
  - grizzly
  - polar bear
  seals:
  - sea lion
  - walrus
"""

EXPECT_CATS_BIG_1 = "tiger\n"

EXPECT_YAML_CANINES = """dogs:
- wolf
- dog
- fox
bears:
- grizzly
- polar bear
seals:
- sea lion
- walrus
"""

EXPECT_YAML_JAGUAR2PANTHER = """felines:
  cats:
    big:
    - lion
    - tiger
    - panther
    small:
    - ocelot
    - domestic cat
canines:
  dogs:
  - wolf
  - dog
  - fox
  bears:
  - grizzly
  - polar bear
  seals:
  - sea lion
  - walrus
"""

REPLACEMENT_YAML_CATS_TAXONOMY = """panthera:
  leo: lion
  tigris: tiger
  pardus: leopard
felis:
  silvestris: wildcat
  catus: domestic cat
"""

EXPECT_YAML_CATS_TAXONOMY = """felines:
  cats:
    panthera:
      leo: lion
      tigris: tiger
      pardus: leopard
    felis:
      silvestris: wildcat
      catus: domestic cat
canines:
  dogs:
  - wolf
  - dog
  - fox
  bears:
  - grizzly
  - polar bear
  seals:
  - sea lion
  - walrus
"""

EXPECT_CONVERTED_JSON_ANIMALS = """{
  "felines": {
    "cats": {
      "big": [
        "lion",
        "tiger",
        "jaguar"
      ],
      "small": [
        "ocelot",
        "domestic cat"
      ]
    }
  },
  "canines": {
    "dogs": [
      "wolf",
      "dog",
      "fox"
    ],
    "bears": [
      "grizzly",
      "polar bear"
    ],
    "seals": [
      "sea lion",
      "walrus"
    ]
  }
}
"""

INPUT_JSON_ANIMALS = EXPECT_CONVERTED_JSON_ANIMALS
EXPECT_CONVERTED_YAML_ANIMALS = INPUT_YAML_ANIMALS

EXPECT_JSON_CANINES = """{
  "dogs": [
    "wolf",
    "dog",
    "fox"
  ],
  "bears": [
    "grizzly",
    "polar bear"
  ],
  "seals": [
    "sea lion",
    "walrus"
  ]
}"""

INPUT_JSON_AURORA = """{
  "aurora": {
    "borealis": "northern lights",
    "australis": "southern lights",
    "sutra": "german darkwave band"
  }
}
"""

EXPECT_JSON_AURORA_AUSTRALIS = '"southern lights"\n'

EXPECT_CONVERTED_YAML_AURORA = """aurora:
  borealis: northern lights
  australis: southern lights
  sutra: german darkwave band
"""

INPUT_ANIMAL_MERGER_1 = """felines:
  cats:
    medium:
    - ocelot
    - serval
    small:
    - domestic cat
canines:
  seals: null
"""

EXPECT_MERGED_ANIMALS_1 = """felines:
  cats:
    big:
    - lion
    - tiger
    - jaguar
    small:
    - domestic cat
    medium:
    - ocelot
    - serval
canines:
  dogs:
  - wolf
  - dog
  - fox
  bears:
  - grizzly
  - polar bear
  seals: null
"""


# vim: fileencoding=utf-8 ts=4 sts=4 sw=4 autoindent expandtab syntax=python:
