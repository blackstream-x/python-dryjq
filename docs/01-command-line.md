# Command line

```bash
usage: dryjq [-h] [--version] [-i] [-d | -v | -q] [-o OUTPUT_FORMAT]
             [--indent {2,4,8}] [--sort-keys] [--separator SEPARATOR]
             [--subscript-indicators SUBSCRIPT_INDICATORS]
             [query] [input_file]
```

## Generic options

-   `-h`, `--help`

    Show the help message and exit.

-   `--version`

    Show the module version and exit.

-   `-i`, `--inplace`

    If used with an input file and in _replace mode_,
    write program output back to the file.

    If no input file is specified, this option has no effect.


## Logging options

-   `-d`, `--debug`

    Output all messages (loglevel DEBUG and above)

-   `-v`, `--verbose`

    Verbose logging (loglevel INFO and above)

-   `-q`, `--quiet`

    Quiet logging (loglevel ERROR and above)


## Output options

!!! note ""

    May be used to control output formatting.

-   `-o` FORMAT, `--output-format` FORMAT

    Specify the output format.
    FORMAT is case-insensitive and may be the start of:

    -   `input`

        Output the same format as the input format. This is the default.

    -   `JSON`

        Output JSON

    -   `YAML`

        Output YAML

    -   `toggle`

        Switch output format to the opposite:
        output JSON when the input format was YAML, or vice versa.

    For example: `-o y`, `-o Ya`, `-o yAm`, `-o yaml` or `-o YAML` all force YAML output,
    but `-o yml` would not be recognized.

-   `--indent` {2|4|8}

    Default output indent is `2`, but it can be changed to `4` or `8`
    using this option.

-   `--sort-keys`

    By default, mapping keys are left in input order. You can sort all mappings
    by using this option.


## Query syntax options

!!! note ""

    May be used to change the separator and/or subscript indicator characters
    recognized in a query.

-   `--separator` SEPARATOR

    The **separator** character. Default is a dot (`.`).

-   `--subscript-indicators` SUBSCRIPT_INDICATORS

    The **subscript indicator** characters. Default: square brackets (`[]`).


## Positional Parameters

1.  _query_

    A query addressing a single datum in the data structure represented
    by the input. Must always start with a **separator**.
    The default value is a single **separator**.

    Can contain an assignment using `=`, which switches operation to _replace mode_.

    See the next page ([Query Syntax](./02-query-syntax.md)) for details.

2.  _input\_file_

    By default, data will be read from standard input.
    Use this parameter to read an input file directly
    (and modify it if values have been replaced
    _and_ the `--inplace` option was used).

