# Query Syntax

!!! caution

    **dryjq** query syntax is _influenced_ by that of
    [yq](https://mikefarah.gitbook.io/yq/) and [jq](https://stedolan.github.io/jq/),
    but not exactly the same.
    
    In contrast to these, dryjq’s functionality has been radically reduced,
    so its query syntax could be kept less complex.


All queries **must** have an address part and **may** have a value part,
separated by an equal sign (`=`).

Default operation is in _extract mode_.
If the query has a value part, **dryjq** operates in _replace mode_.

## Addresses

!!! note "Data structures"

    Data structures in JSON or YAML represent a
    [tree](https://en.wikipedia.org/wiki/Tree_(data_structure))
    consisting of a combination of lists, mappings
    and scalar values (ie. strings, numbers, boolean values or `null`).
    
    Scalar values are always "leaf nodes" in the tree and have a value, but no children.
    
    Lists and mappings are "branch nodes" and have children, but no value themselves.


An address is a combination of mapping keys and/or list indexes separated
by **separators** (by default:  ASCII dot, `.`) that addresses a node
in the data structure tree.

All addresses **must** start with a **separator**.

The shortest possible address is one **separator** only and represents
the root node of the tree.

!!! note "Whitespace is ignored"
    
    Whitespace inside an address is ignored (unless quoted)

### List items

List items are addressed using their numerical index
(always starting at `0`) enclosed in **subscript indicator** characters
(by default: square brackets, `[]`).

Negative values are allowed too. They count backwards from the end of a list,
see note #3 at
[The Python Standard Library documentation of common sequence operations](https://docs.python.org/3/library/stdtypes.html#common-sequence-operations):
`-1` is the last item in a list, `-2` the next-to-last, etc.

!!! tip "Mapping keys in subscript"

    Mapping items _can_ also be addressed by their keys enclosed in a subscript.

### Mapping items

Mapping items are addressed by their keys
exactly how they appear in a YAML file.

!!! note ""

    Mapping keys **must** be scalar values.

If a mapping key contains any of the following characters,
it should be enclosed in single(`'`) or double (`"`) quotes
_probably in addition to the quotes added on the command line to prevent shell interpolation_:

-   due to possible misinterpretation by the YAML parser:
    -   colon (`:`) → YAML indicator for mapping key/value pairs
    -   pound sign (`#`) → YAML comment sign
-   if not inside a subscript:
    -   the **separator** character
    -   equal sign (`=`)
-   space (` `), 
-   **subscript indicator** characters

The same applies if the key would be interpreted as another type by
the YAML parser (eg. literal values `yes` or `true` are interpreted as a Boolean
having the value `True`, whereas `"yes"` is interpreted as the regular string `yes`).

!!! caution "Caveat: float mapping keys with the default separator"

    Addressing a mapping key that is a floating point number
    while keeping the default **separator** character
    has to be done _in subscript_ because otherwise the dot in 
    the number’s notation would be misinterpreted as a separator.

## Replacement values

When the address is followed by an equal sign (`=`) all remaining data will be
used to determine the replacement. Whitespace at start and end is stripped,
then the result is read using the YAML parser to determine the final
replacement value.

!!! tip ""

    This way, not only scalar values (strings, numbers, Booleans, `null`)
    can be used as a replacement value, but also lists and mappings,
    or even nested data structures.

