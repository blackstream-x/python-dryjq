# dryjq.queries module

> Translate queries into **[ParsedQuery][dq-parsedquery]** instances


## Item dataclasses

### class dryjq.queries.BaseItem

!!! note ""

    **BaseItem**()

> Base class for all items provided from the **[Itemizer][dq-itemizer]** class
> and interpreted by the **[Parser][dq-parser]** class.

Instances of **BaseItem** and its subclasses are immutable and hashable.


### class dryjq.queries.DelimiterItem

!!! note ""

    **DelimiterItem**()

> **[BaseItem][dc-baseitem]** subclass and base class for start and end of a query.


### class dryjq.queries.QueryStartItem

!!! note ""

    **QueryStartItem**()

> **[DelimiterItem][dc-delimiteritem]** subclass for the start of a query.


### class dryjq.queries.QueryEndItem

!!! note ""

    **QueryEndItem**()

> **[DelimiterItem][dc-delimiteritem]** subclass for the end of a query.


### class dryjq.queries.ItemWithContent

!!! note ""

    **ItemWithContent**(_content: **str**_)

> **[BaseItem][dc-baseitem]** subclass for items with text content.


#### ItemWithContent instance attribute

*   **content** → **str**

    > the contained text.


### class dryjq.queries.AssignmentItem

!!! note ""

    **AssignmentItem**(_content: **str**_)

> **[ItemWithContent][dc-itemwithcontent]** subclass indicating an assignment, initialized with `"="`
> (see **[Itemizer][dq-itemizer]** class attribute **equals\_sign**).


### class dryjq.queries.LiteralItem

!!! note ""

    **LiteralItem**(_content: **str**_)

> **[ItemWithContent][dc-itemwithcontent]** subclass indicating a literal initialized with text content
> including quotes. The content of **LiteralItem** instances before the first
> **[AssignmentItem][dc-assignmentitem]** is used to determine the _path_ attribute of the
> **[ParsedQuery][dq-parsedquery]** instances constructed by the
> **parse\_query()** method of **[Parser][dq-parser]** instances.


### class dryjq.queries.SeparatorItem

!!! note ""

    **SeparatorItem**(_content: **str**_)

> **[ItemWithContent][dc-itemwithcontent]** subclass indicating a separator,
> initialized with the separator character
> (default: `"."`, see [dryjq.commons][commons].**DEFAULT\_SEPARATOR\_CODEPOINT**).


### class dryjq.queries.SubscriptOpenerItem

!!! note ""

    **SubscriptOpenerItem**(_content: **str**_)

> **[ItemWithContent][dc-itemwithcontent]** subclass indicating a subscript opener,
> initialized with the subscript opening character
> (default: `"["`, see [dryjq.commons][commons].**DEFAULT\_SUBSCRIPT\_INDICATORS**).


### class dryjq.queries.SubscriptCloserItem

!!! note ""

    **SubscriptCloserItem**(_content: **str**_)

> **[ItemWithContent][dc-itemwithcontent]** subclass indicating a subscript closer,
> initialized with the subscript closing character
> (default: `"]"`, see [dryjq.commons][commons].**DEFAULT\_SUBSCRIPT\_INDICATORS**).


### class dryjq.queries.WhitespaceItem

!!! note ""

    **WhitespaceItem**(_content: **str**_)

> **[ItemWithContent][dc-itemwithcontent]** subclass indicating whitespace,
> initialized with a whitespace character
> (see **[Itemizer][dq-itemizer]** class attribute **whitespace**).


## Exceptions

### dryjq.queries.IllegalStateError

!!! note ""

    **IllegalStateError**(_message: **str**, offending\_item: **[BaseItem][dc-baseitem]**, parser\_phase: **Optional\[str\]** = `None`, position: **int** =`0`_)

> Raised by **[Parser][dq-parser]** instances in case of an illegal state
> (eg. when a query does not start with a separator).


#### IllegalStateError instance attributes

*   **message** → **str**

    > a message giving detailed information about the error

*   **offending\_item** → **[BaseItem][dc-baseitem]**

    > the offending item

*   **parser\_phase** → **Optional\[str\]**: `None`

    > the phase of the parser when the error occurred

*   **position** → **int**: `0`

    > the position of the offending item (counting from **`1`**, not `0`)


### dryjq.queries.MalformedQueryError

!!! note ""

    **MalformedQueryError**(_character\_position: **int** = `0`_)

> Base class for exceptions raised by **[Itemizer][dq-itemizer]** instances
> if the itemizer detects an error in the query.


#### MalformedQueryError instance attribute

*   **character\_position** → **int**: `0`

    > a character position relevant for the error (counting from **`1`**, not `0`)


### dryjq.queries.UnclosedQuoteError

!!! note ""

    **UnclosedQuoteError**(_character\_position: **int** =`0`_)

> **[MalformedQueryError][dq-malformedqueryerror]** subclass,
> raised by **[Itemizer][dq-itemizer]** instances
> if a quote is not closed until the end of the query.

> The _character\_position_ attribute contains the character position
> of the opening quote.


### dryjq.queries.UnclosedSubscriptError

!!! note ""

    **UnclosedSubscriptError**(_character\_position: **int** =`0`_)

> **[MalformedQueryError][dq-malformedqueryerror]** subclass,
> raised by **[Itemizer][dq-itemizer]** instances
> if a subscript is not closed until the end of the query.

> The _character\_position_ attribute contains the character position
> of the subscript opener (which is `[` by default).


## class dryjq.queries.Itemizer

!!! note ""

    **Itemizer**(_separator\_codepoint: **Optional\[int\]** = `None`, subscript\_indicators\_pair: **Optional\[[dryjq.commons.CharactersPair][dc-characterspair]\]** = `None`_)

> Itemizes a query for the parser by returning an iterator over item
> (ie. **[BaseItem][dc-baseitem]** subclass) instances.


### Itemizer class attributes

*   **quotes** → **Tuple\[int, int\]**: `(0x22, 0x27)`

    > a tuple containing the codepoints of `"` and `'`

*   **equals\_sign** → **int**: `0x3D`

    > the codepoint of `=`

*   **whitespace** → **Tuple\[int, int, int, int\]**: `(0x09, 0x0A, 0x0D, 0x20)`

    > a tuple containing the codepoints of _Tab_, _Newline_, _Carriage Return_ and _Space_


### Itemizer instance attributes

*   **separator\_codepoint** → **int**: `0x2E`

    > the separator codepoint.

    > If not provided at instantiation time, this attribute is set to the result of
    > ord([d.c][commons].**DEFAULT\_SEPARATOR**) (=`0x2E`).

*   **subscript\_indicators\_pair** → [dryjq.commons.**CharactersPair**][dc-characterspair]: [d.c.**CharactersPair**][dc-characterspair](`"[]"`)

    > the [dryjq.commons.**CharactersPair**][dc-characterspair]
    > instance indicating a subscript.

    > If not provided at instantiation time, this attribute is set to
    > [d.c.**CharactersPair**][dc-characterspair]([d.c][commons].**DEFAULT\_SUBSCRIPT\_INDICATORS**)
    > (=[d.c.**CharactersPair**][dc-characterspair](`"[]"`))


### Itemizer instance public method

#### .itemize()

!!! note ""

    **.itemize**(_original\_query_) → **Iterator\[[BaseItem][dc-baseitem]\]**

> Disassembles the original query into a sequence of items
> and returns an Iterator over the appropriate **[BaseItem][dc-baseitem]** subclass instances. 

> Raises an **[UnclosedQuoteError][dq-unclosedquoteerror]** if a quote is not closed,
> or an **[UnclosedSubscriptError][dq-unclosedsubscripterror]** if a subscript is not closed.


## class dryjq.queries.ParsedQuery

!!! note ""

    **ParsedQuery**(_path: **[serializable\_trees.trees.TraversalPath][st-traversalpath]**, replacement: **Optional\[[serializable\_trees.trees.Tree][st-tree]\]**_)

> Result of a **[Parser][dq-parser]** run, implemented as a dataclass.
> Can be applied to [serializable\_trees.trees.**Tree**][st-tree] instances.

> **ParsedQuery** instances having an empty _path_
> (=[serializable\_trees.trees.**TraversalPath()**][st-traversalpath]) and `None` as _replacement_
> evaluate to `False` in a **bool** context.


### ParsedQuery instance attributes

*   **path** → [serializable\_trees.trees.**TraversalPath**][st-traversalpath]

    > The path specified in the query

*   **replacement** → **Optional\[**[serializable\_trees.trees.**Tree**][st-tree]**\]**: `None`

    > The replacement as a [serializable\_trees.trees.**Tree**][st-tree] instance,
    > or `None` for extracting queries.


### ParsedQuery instance method

#### .apply\_to()

!!! note ""

    **.apply\_to**(_original: **[serializable\_trees.trees.Tree][st-tree]**_) → **[serializable\_trees.trees.Tree][st-tree]**

> Applies the query to the _original_ [serializable\_trees.trees.**Tree**][st-tree] instance
> and returns the result as another [serializable\_trees.trees.**Tree**][st-tree] instance.

> This method never changes the original tree, but always operates on a deep copy.


## class dryjq.queries.Parser

!!! note ""

    **Parser**()

> Parses a query into a **[ParsedQuery][dq-parsedquery]** instance


### Parser class attributes

_Constants defining identifiers for the parser phases_

*   **phase\_before\_start**  → **str**: `"before start"`
*   **phase\_started** → **str**: `"started"`
*   **phase\_address** → **str**: `"address"`
*   **phase\_replacement** → **str**: `"replacement"`
*   **phase\_ended** → **str**: `"ended"`


### Parser instance attribute

*   **phase** → **str**

    > The current phase of the parser (one of the class-defined constants).


### Parser instance public method

#### .parse\_query()

!!! note ""

    **.parse\_query**(_original\_query: **str**, separator\_codepoint: **Optional\[int\]** = `None`, subscript\_indicators\_pair: **Optional\[[dryjq.commons.CharactersPair][dc-characterspair]\]** = `None`_) → **[ParsedQuery][dq-parsedquery]**

> Parses a query and returns a **[ParsedQuery][dq-parsedquery]** instance

> Internally, a new **[Itemizer][dq-itemizer]** instance is initialized with _separator\_codepoint_
> and _subscript\_indicators\_pair_, then its **.itemize()** method is called
> with _original\_query_.

> A **[serializable\_trees.trees.TraversalPath][st-traversalpath]** instance
> is built from the items belonging to the address part of the query,
> and used as the _path_ attribute of the **[ParsedQuery][dq-parsedquery]** instance.

> If the query contains a replacement part, a **[serializable\_trees.trees.Tree][st-tree]**
> instance is built from the items belonging to it,
> and used as the _replacement_ attribute of the **[ParsedQuery][dq-parsedquery]** instance;
> else, the _replacement_ attribute is set to `None`.

> Raises an **[IllegalStateError][dq-illegalstateerror]** on errors detected by the parser
> (eg. a query not starting with a separator).


[commons]: commons.md
[dc-baseitem]: #class-dryjqqueriesbaseitem
[dc-delimiteritem]: #class-dryjqqueriesdelimiteritem
[dc-itemwithcontent]: #class-dryjqqueriesitemwithcontent
[dc-assignmentitem]: #class-dryjqqueriesassignmentitem
[dc-characterspair]: commons.md#class-dryjqcommonscharacterspair
[dq-illegalstateerror]: #dryjqqueriesillegalstateerror
[dq-malformedqueryerror]: #dryjqqueriesmalformedqueryerror
[dq-unclosedquoteerror]: #dryjqqueriesunclosedquoteerror
[dq-unclosedsubscripterror]: #dryjqqueriesunclosedsubscripterror
[dq-itemizer]: #class-dryjqqueriesitemizer
[dq-parsedquery]: #class-dryjqqueriesparsedquery
[dq-parser]: #class-dryjqqueriesparser
[st-traversalpath]: https://blackstream-x.gitlab.io/serializable-trees/module_reference/trees/#traversalpath
[st-tree]: https://blackstream-x.gitlab.io/serializable-trees/module_reference/trees/#tree

