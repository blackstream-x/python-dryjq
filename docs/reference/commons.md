# dryjq.commons module

> Common constants and helper classes

## Module-level constants

dryjq.commons.**DEFAULT\_INDENT** → **int**: `2`

> Default indent level for JSON or YAML output.

dryjq.commons.**DEFAULT\_SEPARATOR** → **str**: `"."`

> (ASCII code `0x2E` hex / `46` dec) → Default separator for queries.

dryjq.commons.**DEFAULT\_SUBSCRIPT\_INDICATORS** → **str**: `"[]"`

> Default characters pair indicating an enclosed subscript

dryjq.commons.**FORMAT_INPUT** → **str**: `"input"` 

> used to preserve the input format (default behaviour of the `dryjq` executable).

dryjq.commons.**FORMAT_JSON** → **str**: `"JSON"`

> indicates JSON format, may be used to force output in JSON format.

dryjq.commons.**FORMAT_YAML** → **str**: `"YAML"`

> indicates YAML format, may be used to force output in YAML format.

dryjq.commons.**FORMAT_TOGGLE** → **str**: `"toggle"`

> used to toggle the format between JSON and YAML
> (default behaviour of the `dryjq.convert` executable).

dryjq.commons.**SUPPORTED_FORMATS** → **Tuple\[str, str\]**: (**FORMAT_JSON**, **FORMAT_YAML**)

> a tuple containing the supported formats.


## class dryjq.commons.CharactersPair

!!! note ""

    **CharactersPair**(_source: **str**_)

> A pair of characters initialized from a 1- or 2-characters-string

**CharactersPair** instances are immutable and hashable.
If initialized with a single character, the **first** and **last** codepoints
are equal.

**CharactersPair** instances compare equal if their **both** attributes are equal.

Assuming `characters_pair` is a **CharactersPair** instance,
the following statements always evaluate to `True`:

```python
>>> characters_pair.first == ord(characters_pair.source[0])
True
>>> characters_pair.last == ord(characters_pair.source[-1])
True
```

#### CharactersPair instance attributes

*   **source** → **str**

    > source string of the instance.

*   **first** → **int**

    > codepoint of the first character.

*   **last** → **int**

    > codepoint of the last character.

*   **open** → **int**

    > alias of **first**.

*   **close** → **int**

    > alias of **last**.

*   **both** → **Tuple\[int, int\]**

    > a tuple containing (**first**, **last**).

