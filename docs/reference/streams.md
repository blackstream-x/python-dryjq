# dryjq.streams module

> Streams and files handling

## class dryjq.streams.StreamReader

!!! note ""

    **StreamReader**(_stream\_io: **TextIO**_)

> De-serialize the data structure from the stream into a
> [serializable\_trees.trees.**Tree**][st-tree] instance, manipulate it internally and
> serialize it in the requested format. Write output to **sys.stdout**.

### StreamReader instance attributes

*   **input\_contents** → **str**

    > the original stream contents. 

*   **input\_format** → **str**

    > the detected original stream contents format
    > (one of [dryjq.commons](commons.md).**FORMAT\_JSON**
    > and [dryjq.commons](commons.md).**FORMAT\_YAML**). 

*   **output\_contents** → **str**

    > the serialized data structure.

*   **output\_format** → **str**

    > the output format for serialization (see **input_format**).

*   **stream\_io** → **TextIO**

    > the original stream itself.


### StreamReader instance methods

#### .set\_serialization\_format()

!!! note ""

    **.set\_serialization\_format**(_output\_format: **str** = [dryjq.commons](commons.md).**FORMAT\_INPUT**, indent: **int** = [dryjq.commons](commons.md).**DEFAULT_INDENT**, sort\_keys: **bool** = `False`_) → **None**

> Set the serialization format for the internal data structure.

#### .execute\_merge()

!!! note ""

    **.execute\_merge**(_updating\_data\_structure: **[serializable\_trees.trees.Tree][st-tree]**_) → **None**

> Merge the provided
> _updating\_data\_structure_ [serializable\_trees.trees.**Tree**][st-tree] instance
> into the internal data structure.


#### .execute\_single\_query()

!!! note ""

    **.execute\_single\_query**(_parsed\_query: **[dryjq.queries.ParsedQuery][dq-parsedquery]**_) → **None**

> Filter the internal data structure by executing the provided
> [dryjq.queries.**ParsedQuery**][dq-parsedquery] instance’s **.apply\_to()** method
> on the internal data structure and
> setting the internal data structure to the result of that operation.


#### .write\_output()

!!! note ""

    **.write\_output**() → **None**

> Write **output\_contents** to standard output, ensuring it ends with
> exactly one Newline character (`'\n'`).


## class dryjq.streams.FileWriter

!!! note ""

    **FileWriter**(_stream\_io: **TextIO**_)

> **[StreamReader][ds-streamreader]** subclass operating on files in read/write mode
> (ie. opened in mode `'r+'`).

### FileWriter instance additional method

#### .write\_output()

!!! note ""

    **.write\_output**() → **None**

> Write the serialized data structure back to the file, replacing
> its former contents.
> If the file contents would not change, or if the output format is different to
> the input format, do nothing and print an error message instead.
> This measure was introduced to prevent writing JSON data to a YAML file
> or vice versa.


[dq-parsedquery]: queries.md#class-dryjqqueriesparsedquery
[ds-streamreader]: #class-dryjqstreamsstreamreader
[st-traversalpath]: https://blackstream-x.gitlab.io/serializable-trees/module_reference/trees/#traversalpath
[st-tree]: https://blackstream-x.gitlab.io/serializable-trees/module_reference/trees/#tree
