# dryjq package contents

The **dryjq** package consists of several submodules:

-   [commandline](commandline.md) → common functionality for the command line programs
-   [commons](commons.md) → common constants and helper classes
-   [queries](queries.md) → parser for **dryjq** queries
-   [streams](streams.md) → file and stream handling (JSON and YAML readers and writers)
-   special modules:
    -   `__init__` → package core module defining the version (`dryjq.__version__`)
    -   `convert`, `merge`, and `__main__` → command line interface modules

