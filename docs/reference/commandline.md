# dryjq.commandline module

> Common constants and functionality for the command line scripts

## Module-level constants

dryjq.commandline.**RETURNCODE\_OK** → **int**: `0`

> returned by the script on successful execution.

dryjq.commandline.**RETURNCODE\_ERROR** → **int**: `1`

> returned by the script in the case of an error.


## class dryjq.commandline.Program

!!! note ""

    **Program**(_args: **Optional\[List\[str\]\]** = `None`_)

> Parses command line arguments (either provided via the _args_ list
> or read from [sys.argv](https://docs.python.org/3/library/sys.html#sys.argv))
> and stores the result in the **arguments** instance attribute.
> Initializes logging.

> The **Program** class can be subclassed, as done in the `convert` module.
> Different behavior of the subclass is achieved by overwriting the class attributes.


### Program class attributes

*   **name** → **str**: `"dryjq"`

    > the name of the executable

*   **description** → **str**: `"Drastically Reduced YAML / JSON Query"`

    > the description of the program in the usage/help message

*   **query\_option** → **bool**: `True`
    
    > Whether the program allows specifying the query.

    > If this attribute is `True`, the `--separator` and `--subscript-indicators`
    > options may be used to change the query syntax.

*   **merge\_program** → **bool**: `False`

    > If this attribute is `True`, the program merges data structures
    > (defined either in the files from additional command line parameters
    > after the first file name, or in standard input) into the input file.

*   **modify\_in\_place\_option** → **bool**: `True`

    > Whether direct modification of
    > input files is supported. If `True`, the `-i` resp. `--inplace` option
    > may be used.

*   **default\_output\_format** → **str**: [dryjq.commons](commons.md).**FORMAT_INPUT** (=`"input"`)
    
    > Output results in the same format as detected in the input

*   **allowed\_formats** → **Dict\[str, str\]**

    > a dict mapping input formats to their descriptions


### Program instance attribute

*   **arguments** → **[argparse.Namespace](https://docs.python.org/3/library/argparse.html#argparse.Namespace)**

    > the parsed command line arguments
    > as returned by the **parse_args()** method of the argument parser,
    > see the [Standard Library documentation](https://docs.python.org/3/library/argparse.html#parsing-arguments)).


### Program class method

#### .complete\_format()

!!! note ""

    **.complete\_format**(_input\_format: **str**_) → **str**

> The _output format_ can be set to any of the allowed formats
> (see the [dryjq.commons](commons.md).**FORMAT\_\*** constants)
> using the `-o` resp. `--output-format` option.

> This method returns the exact output format matching the provided
> case-insensitive beginning of it,
> so you can eg. enforce the `JSON` output format by specifying `-o j`.


### Program instance public method

#### .execute()

!!! note ""

    **.execute**(_parsed\_query: **Optional\[[dryjq.queries.ParsedQuery][dq-parsedquery]\]** = `None`_) → **int**

> Execute the program according to the provided command line arguments.

> The _query_ provided on the command line is not processed here, but must be parsed into a
> [dryjq.queries.**ParsedQuery**][dq-parsedquery] instance.

> If either `None` or an empty [dryjq.queries.**ParsedQuery**][dq-parsedquery] instance
> (_path_ == [serializable\_trees.trees.**TraversalPath()**][st-traversalpath]
> and _replacement_ == `None`) is provided,
> the data structure is simply passed through, no extraction or replacement
> will be performed (eg. for format conversion).

> If the **merge\_program** class attribute is `True`,
> data structures are merged instead
> (see `dryjq.merge -h` output for usage information).

> Returns the exit code for the program.


[dq-parsedquery]: queries.md#class-dryjqqueriesparsedquery
[st-traversalpath]: https://blackstream-x.gitlab.io/serializable-trees/module_reference/trees/#traversalpath
[st-tree]: https://blackstream-x.gitlab.io/serializable-trees/module_reference/trees/#tree
