# Use cases

## Example input file

[edibles.yaml](./edibles.yaml)

```yaml
herbs:
  common:
    - basil
    - oregano
    - parsley
    - thyme
  disputed:
    - anise
    - coriander
vegetables:
  common:
    - bell peppers
    - carrots
    - potatoes
    - tomatoes
  disputed:
    - Brussels sprouts
    - celery
    - garlic
    - spinach
  trend:
    - eggplant
    - zucchini
```


## Extract Mode

### Get a single value

Command:

```bash
dryjq .herbs.common[1] edibles.yaml
```

Result:

```yaml
oregano
```

### Get a subtree

Command:

```bash
dryjq .herbs edibles.yaml
```

Result:

```yaml
common:
- basil
- oregano
- parsley
- thyme
disputed:
- anise
- coriander
```

## Replace Mode

### Change a single value

Command:

```bash
dryjq ".vegetables.disputed[0] = capers" edibles.yaml
```

Result:

```yaml
herbs:
  common:
  - basil
  - oregano
  - parsley
  - thyme
  disputed:
  - anise
  - coriander
vegetables:
  common:
  - bell peppers
  - carrots
  - potatoes
  - tomatoes
  disputed:
  - capers
  - celery
  - garlic
  - spinach
  trend:
  - eggplant
  - zucchini
```

### Change a subtree

Command:

```bash
dryjq ".vegetables.disputed = None, I eat all vegetables ;-)" edibles.yaml
```

Result:

```yaml
herbs:
  common:
  - basil
  - oregano
  - parsley
  - thyme
  disputed:
  - anise
  - coriander
vegetables:
  common:
  - bell peppers
  - carrots
  - potatoes
  - tomatoes
  disputed: None, I eat all vegetables ;-)
  trend:
  - eggplant
  - zucchini
```

## Format changes

You can use the options from the
[Output options group](01-command-line.md#output-options)
to change the output format.
The following changes are supported:

### Change representation format

-   Toggle between JSON and YAML: `--output-format toggle`
    or simply use `dryjq.convert`
-   Change format to JSON explicitly: `--output-format JSON`
-   Change format to YAML explicitly: `--output-format YAML`

### Change indentation width

Use `--indent` with the desired indent width (2, 4 or 8).

### Sort all mapping keys

Use `--sort-keys`


## Merge data structures

You can merge secondary data structures into the primary data structure
defined in the input file using the
`dryjq.merge` command.
