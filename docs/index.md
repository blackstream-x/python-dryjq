# Drastically Reduced YAML / JSON Query

## Abstract

Python module and command line program for accessing
the data structure represented by a JSON or YAML file.

**dryjq** operates in two modes, depending on the query:

1.  Output the data structure or a part of it (_extract mode_).
2.  Replace a single value or a subtree
    and output the modified data structure (_replace mode_).

The program reads from standard input and writes to standard output,
so it is well suited for usage in shell pipelines.

!!! note "Notable differences from jq and yq"

    **dryjq** operates on the data structure
    (using PyYAML internally), not the textual representation.
    This implies the following:

    -   YAML comments are ignored completely
    -   Extra whitespace is not preserved
    -   Input indent width is not preserved

    **dryjq** query addresses are always key- or index-based
    if they contain more parts than the passthru address
    (a single **separator** character – by default `.`),
    so each address part is considered a mapping key or a list index.
    More complex addressing is not supported,
    so less quoting is required:

    -   in **jq**, a key containing a dash (`-`) has to be quoted,
        whereas in **dryjq** quotes are not required in this case.

    You cannot apply several changes _at once_
    using dryjq. However, you can chain multiple
    dryjq commands in a shell pipeline to reach a similar result.

## Installation

See the [Project page on PyPI](https://pypi.org/project/dryjq/)
for available releases.

The latest version can be installed using

```bash
pip install --upgrade dryjq
```

!!! hint "Recommended installation type"

    Installation in a
    [virtual environment](https://docs.python.org/3/tutorial/venv.html)
    is strongly recommended.

