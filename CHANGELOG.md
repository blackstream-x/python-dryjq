# Changelog for dryjq


## v1.9.1

_Released: 2023-09-19_

- Refactored the internal data structure handling to use the [serializable-trees package](https://pypi.org/project/serializable-trees/)


## v1.3.1

_Released: 2023-08-22_

- With --version, print only the release and not the program name
- Modularized the pipeline (use the [building-blocks project](https://gitlab.com/blackstream-x/building-blocks))


## v1.3.0

_Released: 2023-07-03_

No source changes

- Update PyYAML dependency to 6.0
- Update links
- Remove Python 3.7 support


## v1.2.0

_Released: 2023-02-16_

- Use up-to-date build mechanism (TOML only)


## v1.1.0

_Released: 2023-02-16_

- Added merge functionality


## v1.0.1

_Released: 2023-02-14_

- Switch to Python 3.11 build image


## v1.0.0

_Released: 2022-09-08_

- Completed documentation and tests
- Bugfixes


## v0.9.0

_Released: 2022-08-30_

- Factored out constants and helper functions to the `dryjq.commons` module
- Removed file locking (platform dependent and probably unreliable)
- Added reference documentation and tests for the core modules


## v0.8.0

_Released: 2022-08-10_

- Renamed options for consistency with other tools
- Improved naming of operation modes


## v0.7.0

_Released: 2022-08-08_

- Added `dryjq.convert` command
- Prevent writing files when the format changes


## v0.6.0

_Released: 2022-08-05_

- Refactored core functionality into the modules `access`, `queries` and `streams`
- Added more tests


## v0.5.0

_Released: 2022-08-05_

Factored out the handlers and queries modules
More tests including file reading and modifications in place


## v0.4.0

_Released: 2022-08-03_

- Add documentation
- Tokenizer bugfix: Fixed subscript interpretation
- Refactored the cli module
- Added tests


## v0.3.1 (Bugfix)

_Released: 2022-08-01_

- Fixed in-place file modifications


## v0.3.0 (YANKED)

_Released: 2022-08-01_

---
> :warning: **Contains a bug in the file handler**

> Please use a newer version instead.

---

- New query parser
- Enabled value changes


## v0.2.0

_Released: 2022-07-31_

- Relax dependency on PyYAML (allow 5.4.1 and newer)
- Allow JSON input and output
- Titlecase the project name


## v0.1.1

_Released: 2022-07-30_

- Documentation updates
- Made command line options really optional


## v0.1.0

_Released: 2022-07-30_

Initial release
